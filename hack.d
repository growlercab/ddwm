//###################################################
// File: hack.d
// Created: 2014-12-22 16:38:40
// Modified: 2015-02-21 13:46:29
//
// See LICENSE file for license and copyright details
//###################################################

import std.stdio;
import std.string;
import std.conv;
import std.algorithm;
import std.container;
import std.traits;

union Arg {
	int i;
	uint ui;
	float f;
    string[] s;
	const void *v;
}
auto makeArg(T)(T val) {
    static if(isIntegral!T) {
        static if(isSigned!T) {
            Arg ret = {i:val};
        } else {
            Arg ret = {ui:val};
        }
    } else static if(isFloatingPoint!T) {
        Arg ret = {f:cast(float)(val)};
    } else static if(isArray!T && isSomeString!(ForeachType!T)) {
        Arg ret = {s:cast(string[])(val)};
    } else {
        Arg ret = {v:cast(void*)(val)};
    }
    return ret;
}

struct Button {
	uint click;
	uint mask;
	uint button;

    void function(const Arg* a) func;
    const Arg arg;

    this(uint click, uint mask, uint button, void function(const Arg* a) func) {
        this(click, mask, button, func, null);
    }
    this(T)(uint click, uint mask, uint button, void function(const Arg* a) func, T arg) {
        this.click = click;
        this.mask = mask;
        this.button = button;
        this.func = func;
        this.arg = makeArg(arg);
    }    
}

void test_int(const Arg* a) {
    int val = a.i;
    writefln("test_int:%s", val);
}

void test_uint(const Arg* a) {
    uint val = a.ui;
    writefln("test_uint:%s", val);
}


void test_float(const Arg* a) {
    float val = a.f;
    writefln("test_float:%s", val);
}

void test_string(const Arg* a) {
    string[] val = cast(string[])(a.s);
    writefln("test_string:%s", val);
}


void test_voidptr(const Arg* a) {
    const void* val = a.v;
    writefln("test_voidptr:%s", val);
    int vali = *cast(const int*)(a.v);
    writefln("\t--> As integer: %s", vali);
}

void test_empty(const Arg* a) {
    uint vali = a.i;
    uint valui = a.ui;
    float valf = a.f;
    const string[] vals = a.s;
    const void* valptr = a.v;
    writefln("test_empty:i:%s, ui:%s, f:%s, s:%s, v:%s", vali, valui, valf, vals, valptr);
}

void main()
{
    int intptr = 20;

    Button[] buttons = [
        Button(1, 1, 1, &test_empty),
        Button(2, 2, 2, &test_int, 10),
        Button(2, 2, 2, &test_float, 1.123),
        Button(2, 2, 2, &test_string, ["a string", "another string"]),
        Button(2, 2, 2, &test_voidptr, &intptr)
            ];


    foreach(but; buttons) {
        but.func(&but.arg);
    }

}


