# DDWM #

Welcome to DDWM, a D port of the lightweight and efficient Dynamic Window Manager (DWM) that sucks less. More information on DWM can be found at the official website here: http://dwm.suckless.org/.

Information about the D Programming Language can be found here: http://dlang.org.

**NOTE:** DDWM is the original DWM code simply ported to D, however, it cannot be considered stable. Use DWM from suckless if you want a stable window manager.

## Status update 2016-03-09 ##

DDWM hasn't been updated for a while because it is working well enough for me day-to-day and I'm busy on other projects. If you're looking for a D window manager project that may be more actively developed then try cobox-wm:

https://github.com/faianca/cobox-wm

## How to build ##

DDWM uses `dub` for now but this may change. It also depends on libX11 from the Deimos repositories, available here: https://github.com/D-Programming-Deimos/libX11

### 1. Download libX11 from Deimos ###

Deimos.libX11 provides the X bindings used by DDWM. 

1. cd /your/working/area
2. git clone https://github.com/D-Programming-Deimos/libX11
3. dub add-local libX11 0.0.1

### 2. Download and build DDWM ###

1. cd /your/working/area
2. git clone https://bitbucket.org/growlercab/ddwm.git
3. cd ddwm
    * [OPTIONAL] git checkout cport
4. dub build --compiler=dmd --build=release-nobounds
    * [OPTIONAL] add `-cxinerama` if you wish to use the Xinerama libraries for multiple monitor support.

The ```cport``` version is the original DWM code with minimal changes to convert it to D, thus, it will be more stable.

### How to invoke DDWM ###

Set `/your/work/area/ddwm/build-artefacts/ddwm` to be your window manager of choice. For example, create a .xinitrc file and run X from the command line:

```
$ echo exec /your/work/area/ddwm/build-artefacts/ddwm > ~/.xinitrc
$ startx
```

## DDWM Tutorial ##

DDWM uses the same keybindings and controls as DWM so anything that works in DWM should (in theory) work with DDWM.

See http://dwm.suckless.org/tutorial for details on how to use DDWM.



## Background ##

DDWM is still a work in progress, however, the development has been divided into two stages. The first stage was the `cport`, which tries remain as close to the original C code as possible. This stage now works to my satisfaction and exists on the `cport` branch. It will not be maintained beyond bug fixes.

The second stage is currently in development and builds upon `cport` with more idiomatic D style coding. 


## Why bother? ##

Good question! Here's my reasons...

1. To learn D and experiment with the language
2. To find out how easy it is to port C to D ... 
    * it's really easy as you can see comparing the `cport` version with the code in orig-dwm.
3. To find out how easy it is to mix D and C libraries ... 
    * it's even easier than porting C to D.
4. To use a window manager that is written in D.