//#####################################################
// Author: stewart
// File: Xinerama.d
// Created: 2014-12-30 12:31:05
// Modified: 2014-12-30 13:02:11 (stewart)
// 
// See LICENSE file for license and copyright details.
//#####################################################

/*

Copyright 2003  The Open Group

Permission to use, copy, modify, distribute, and sell this software and its
documentation for any purpose is hereby granted without fee, provided that
the above copyright notice appear in all copies and that both that
copyright notice and this permission notice appear in supporting
documentation.

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
OPEN GROUP BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of The Open Group shall not be
used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization from The Open Group.

*/
//#ifndef _Xinerama_h
//#define _Xinerama_h

// #include <X11/Xlib.h>

import deimos.X11.Xlib;

struct XineramaScreenInfo {
   int   screen_number;
   short x_org;
   short y_org;
   short width;
   short height;
} 

// _XFUNCPROTOBEGIN

extern(C) Bool XineramaQueryExtension (
   Display *dpy,
   int     *event_base,
   int     *error_base
) nothrow @nogc @system;

extern(C) Status XineramaQueryVersion(
   Display *dpy,
   int     *major_versionp,
   int     *minor_versionp
) nothrow @nogc @system;

extern(C) Bool XineramaIsActive(Display *dpy) nothrow @nogc @system;


/*
   Returns the number of heads and a pointer to an array of
   structures describing the position and size of the individual
   heads.  Returns NULL and number = 0 if Xinerama is not active.

   Returned array should be freed with XFree().
*/

extern(C) XineramaScreenInfo *
XineramaQueryScreens(
   Display *dpy,
   int     *number
) nothrow @nogc @system;

//_XFUNCPROTOEND

//#endif /* _Xinerama_h */

