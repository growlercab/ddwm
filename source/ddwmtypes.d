//###################################################
// File: ddwm-types.d
// Created: 2014-12-22 17:42:02
// Modified: 2015-02-21 20:58:25
//
// See LICENSE file for license and copyright details
//###################################################

module ddwmtypes;

import std.stdio;
import std.traits;
import std.string : format;
import std.variant;
import deimos.X11.X;
import deimos.X11.Xlib;

import ddwmutil;


// ENUMS
enum { CurNormal, CurResize, CurMove, CurLast }; /* cursor */
enum { SchemeNorm, SchemeSel, SchemeLast }; /* color schemes */
enum { NetSupported, NetWMName, NetWMState,
       NetWMFullscreen, NetActiveWindow, NetWMWindowType,
       NetWMWindowTypeDialog, NetClientList, NetLast }; /* EWMH atoms */
enum { WMProtocols, WMDelete, WMState, WMTakeFocus, WMLast }; /* default atoms */
enum { ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle,
       ClkClientWin, ClkRootWin, ClkLast }; /* clicks */



struct Extnts {
	uint w;
	uint h;
}
// Would use variant.get!T here but it doesn't work for const(Variant)
struct Arg {
    union Vals{
        int ival;
        uint uival;
        float fval;
        string[] sval;
        void* vptr;
    }
    Vals val;
    //Variant val;
    this(TIN)(TIN val) {
        alias T = Unqual!TIN;
        static if(isIntegral!T) {
            this.val.ival = cast(int)(val);
        } else static if(isFloatingPoint!T) {
            this.val.fval = cast(float)(val);
        } else static if(is(TIN == immutable(immutable(char)[])[])) {
            this.val.sval = cast(string[])val;
        } else {
            this.val.vptr = cast(void*)(val);
        }
    }
    @property int i() const {
        return this.val.ival;
    }
    @property void i(int ival) {val.ival = cast(int)(ival);}
    @property uint ui() const {
        return this.val.uival;
    }
    @property void ui(uint ival) {val.uival = cast(uint)(ival);}
    @property float f() const {
        return this.val.fval;
    }
    @property void f(float ival) {val.fval = cast(float)(ival);}
    @property const(string[]) s() const {
        return this.val.sval;
    }
    @property void s(string[] ival) {val.sval = cast(string[])(ival);}

    @property const(void*) v() const {
        return this.val.vptr;
    }
    @property void v(void* ival) {val.vptr = cast(void*)(ival);}
}

/*union Arg {
	int i;
	uint ui;
	float f;
    string[] s;
	const void *v;
}*/
auto makeArg(TIN)(TIN val) {
    alias T = Unqual!TIN;
    return Arg(val);
}

struct Button {
	uint click;
	uint mask;
	uint button;

    void function(const Arg* a) func;
    const Arg arg;

    this(uint click, uint mask, uint button, void function(const Arg* a) func) {
        this(click, mask, button, func, 0);
    }
    this(T)(uint click, uint mask, uint button, void function(const Arg* a) func, T arg) {
        this.click = click;
        this.mask = mask;
        this.button = button;
        this.func = func;
        this.arg = makeArg(arg);
    }
};

struct Client {
	string name;
	float mina, maxa;
	int x, y, w, h;
	int oldx, oldy, oldw, oldh;
	int basew, baseh, incw, inch, maxw, maxh, minw, minh;
	int bw, oldbw;
	uint tags;
	bool isfixed, isfloating, isurgent, neverfocus, oldstate, isfullscreen;
	Client *next;
	Client *snext;
	Monitor *mon;
	Window win;

    /**
     * A range to iterate over the client list via 'next' or 'snext',
     * as specified by the template string.
     * Example:
     * ---
     * auto r = ClientRange!"next"(clientPtr);
     * auto sr = ClientRange!"snext"(clientPtr);
     * ---
     */
    struct ClientRange(string NextField) {
        Client* client;
        @property bool empty() {return client is null;}
        @property auto front() {return client;}
        auto popFront() {
            mixin(`client = client.`~NextField~`;`);
        }
    }
}
/**
 * Help function to create a range for a client list using 'NextField' as the
 * iterating pointer.
 * Params:
 *  head=       First node in the range list
 * Returns:
 *  A client range iterating on the field 'NextField'.
 * Example:
 * ---
 * auto r = client.range!"next"; // Use Client.next pointer
 * auto r = client.range!"snext"; // Use Client.snext pointer
 * ---
 */
auto range(string NextField)(Client* head) {
    return Client.ClientRange!NextField(head);
}
struct Key {
	uint mod;
	KeySym keysym;
    void function(const Arg* a) func;
    const Arg arg;

    this(uint mod, KeySym keysym, void function(const Arg* a) func) {
        this(mod, keysym, func, null);
    }
    this(T)(uint mod, KeySym keysym, void function(const Arg* a) func, T arg) {
        this.mod = mod;
        this.keysym = keysym;
        this.func = func;
        this.arg = makeArg(arg);
    }
}

struct Layout {
	const string symbol;
    void function(Monitor* m) arrange;
}

struct Monitor {
	string ltsymbol;
	float mfact;
	int nmaster;
	int num;
	int by;               /* bar geometry */
	int mx, my, mw, mh;   /* screen size */
	int wx, wy, ww, wh;   /* window area  */
	uint seltags;
	uint sellt;
	uint[2] tagset;
	bool showbar;
	bool topbar;
	Client *clients;
	Client *sel;
	Client *stack;
	Monitor *next;
	Window barwin;
	const(Layout)*[2] lt;


    struct MonitorRange {
        Monitor* monitor;
        @property empty() {
            return monitor is null;
        }
        @property auto front() {
            return monitor;
        }
        auto popFront() {
            monitor = monitor.next;
        }

        int opApply(int delegate(Monitor*) dg)
        {
            int result = 0;
            while(!this.empty)
            {
                result = dg(this.front);
                if(result)
                {
                    break;
                }
                this.popFront;
            }
            return result;
        }
        int opApply(int delegate(size_t, Monitor*) dg) 
        {
            int result = 0;
            size_t ii = 0;
            while(!this.empty)
            {
                result = dg(ii++, this.front);
                if(result)
                {
                    break;
                }
                this.popFront;
            }
            return result;
        }
        

    }


}

auto range(Monitor* head) {
    return Monitor.MonitorRange(head);
}
struct Rule {
	string klass;
	string instance;
	string title;
	uint tags;
	bool isfloating;
	int monitor;
}


__EOF__
interface IHandler {
    abstract void invoke();
}

class Handler(ARG) : IHandler {
private:
    void function(const ARG arg) func;
    ARG arg;
public:
    this(void function(const ARG arg) func, ARG arg) {
        this.func = func;
        this.arg = arg;
    }
    final void invoke() {
        this.func(this.arg);
    }
}
